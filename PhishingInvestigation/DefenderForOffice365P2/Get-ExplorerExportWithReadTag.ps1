<#
.SYNOPSIS
Get a read tag for Explorer exported messages.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.Read
#>

# filter delivered messages
$explorerResults = Import-Csv C:\Temp\ExplorerExport.csv | Where-Object { $_.'Original delivery location' -in 'Inbox/folder', 'Junk folder' }
$ExplorerExportWithReadTag = $explorerResults | ForEach-Object {
    $recipient = $_
    # search for the message
    $msg = Get-MgUserMessage -UserId $recipient.recipients -Filter "internetmessageid eq '$($recipient.'Internet message ID')'" -Property IsRead, InternetMessageId, subject, ReceivedDateTime
    if ($msg -ne $null) {
        $null = Add-Member -InputObject $recipient -NotePropertyName 'MbxMessageId' -NotePropertyValue $($msg.Id) -Force
        $null = Add-Member -InputObject $recipient -NotePropertyName 'IsRead' -NotePropertyValue $($msg.IsRead) -Force
    } else {
        $null = Add-Member -InputObject $recipient -NotePropertyName 'MbxMessageId' -NotePropertyValue '' -Force
        $null = Add-Member -InputObject $recipient -NotePropertyName 'IsRead' -NotePropertyValue 'NotFound' -Force
    }
    $recipient
}
$ExplorerExportWithReadTag | Export-Csv C:\Temp\ExplorerExportWithReadTag.csv -NoTypeInformation -Encoding utf8
