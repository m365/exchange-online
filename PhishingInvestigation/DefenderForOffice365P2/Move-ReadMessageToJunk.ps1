<#
.SYNOPSIS
Based on Explorer export move read messages from inbox to junk folder.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.ReadWrite
#>

# filter delivered and read messages
$explorerResults = Import-Csv C:\Temp\ExplorerExportWithReadTag.csv | Where-Object { $_.IsRead -eq $true -and -not [string]::isnullorempty($_.MbxMessageId) }
$ExplorerExportMoveToJunkMsgResult = $explorerResults | ForEach-Object {
    $recipient = $_
    try {
        # move the message to junk
        $null = Move-MgUserMessage -UserId $recipient.recipients -MessageId $recipient.MbxMessageId -BodyParameter @{DestinationId = 'junkemail' } -ErrorAction stop
        $null = Add-Member -InputObject $recipient -NotePropertyName 'MovedToJunk' -NotePropertyValue $true -Force
    } catch {
        $null = Add-Member -InputObject $recipient -NotePropertyName 'MovedToJunk' -NotePropertyValue 'NotFound' -Force
    }
    $recipient
}
$ExplorerExportMoveToJunkMsgResult | Export-Csv C:\Temp\ExplorerExportMoveToJunkMsgResult.csv -NoTypeInformation -Encoding utf8
