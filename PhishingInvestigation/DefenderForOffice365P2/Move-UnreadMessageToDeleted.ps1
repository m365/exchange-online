<#
.SYNOPSIS
Based on Explorer export move unread messages from inbox to deleted items.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.ReadWrite
#>

# filter delivered and not read messages
$explorerResults = Import-Csv C:\Temp\ExplorerExportWithReadTag.csv | Where-Object { $_.IsRead -eq $false -and -not [string]::isnullorempty($_.MbxMessageId) }
$ExplorerExportDeleteMsgResult = $explorerResults | ForEach-Object {
    $recipient = $_
    try {
        # move the message to deleted items
        $null = Move-MgUserMessage -UserId $recipient.recipients -MessageId $recipient.MbxMessageId -BodyParameter @{DestinationId = 'deleteditems' } -ErrorAction stop
        $null = Add-Member -InputObject $recipient -NotePropertyName 'Deleted' -NotePropertyValue $true -Force
    } catch {
        $null = Add-Member -InputObject $recipient -NotePropertyName 'Deleted' -NotePropertyValue 'NotFound' -Force
    }
    $recipient
}
$ExplorerExportDeleteMsgResult | Export-Csv .\ExplorerExportDeleteMsgResult.csv -NoTypeInformation -Encoding utf8
