<#
.SYNOPSIS
Based on Explorer export move unread messages from inbox to deleted items.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.ReadWrite
#>

# filter delivered and read messages (once again)
$ExplorerExportWithReadTag = Import-Csv C:\Temp\ExplorerExportWithReadTag.csv | Where-Object { $_.IsRead -eq $true -and -not [string]::isnullorempty($_.MbxMessageId) }
$ExplorerExportJunkDeleteMsgResult = $ExplorerExportWithReadTag | ForEach-Object {
    $recipient = $_
    try {
        # search the message
        $msg = Get-MgUserMessage -UserId $recipient.recipients -Filter "internetmessageid eq '$($recipient.'messageId')'" -Property IsRead, InternetMessageId, subject, ReceivedDateTime
        # move the message to deleted items
        $null = Move-MgUserMessage -UserId $recipient.recipients -MessageId $msg.MbxMessageId -BodyParameter @{DestinationId = 'deleteditems' } -ErrorAction stop
        $null = Add-Member -InputObject $trace -NotePropertyName 'Deleted' -NotePropertyValue $true -Force
    } catch {
        $null = Add-Member -InputObject $trace -NotePropertyName 'Deleted' -NotePropertyValue 'NotFound' -Force
    }
    $recipient
}
$ExplorerExportJunkDeleteMsgResult | Export-Csv C:\Temp\ExplorerExportJunkDeleteMsgResult.csv -NoTypeInformation -Encoding utf8
