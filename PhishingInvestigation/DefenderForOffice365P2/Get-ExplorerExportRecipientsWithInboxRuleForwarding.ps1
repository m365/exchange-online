<#
.SYNOPSIS
Get Explorer export with a tag which recipients have forwarding inbox rule set up.

Requires Exchange Online management module connection https://learn.microsoft.com/en-us/powershell/exchange/connect-to-exchange-online-powershell?view=exchange-ps
#>

# Inbox rules run on messages delivered to Inbox folder only
$explorerResults = Import-Csv C:\Temp\ExplorerExport.csv | Where-Object { $_.'Original delivery location' -eq 'Inbox/folder' }
$mbxsWithFwdInboxRule = $explorerResults | ForEach-Object {
    $recipient = $_ 
    # search for inbox rule
    $forwRule = Get-InboxRule -Mailbox $recipient.recipients | 
    Where-Object { $_.enabled -eq $true -and ($_.ForwardAsAttachmentTo.count -gt 0 -or 
            $_.ForwardTo.count -gt 0 -or 
            $_.RedirectTo.count -gt 0) }
    [pscustomobject]@{identity = $recipient.recipients; redirectRule = (($forwRule | Measure-Object).count -gt 0) }
}
$mbxsWithFwdInboxRule | Export-Csv C:\Temp\RecipientsWithInboxRuleForwarding.csv -NoTypeInformation -Encoding utf8
