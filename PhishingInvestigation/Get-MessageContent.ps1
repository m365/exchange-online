<#
.SYNOPSIS
Get message content with messageID and a recipient on the input.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.Read
#>

# Connect-MgGraph

$recipient = 'user@tenant.cz'
$internetMessageId = '<abcdef@mail.cz>'

$invalidFilenameChars = [IO.Path]::GetInvalidFileNameChars() -join ''
$reg = "[{0}]" -f [RegEx]::Escape($invalidFilenameChars)
  
$msg = Get-MgUserMessage -UserId $recipient -Property body, InternetMessageId, subject, ReceivedDateTime -Filter "internetmessageid eq '$internetmessageid'"
Get-MgUserMessageContent -UserId $recipient -MessageId $msg.id -OutFile "C:\Temp\$($msg.subject -replace $reg).eml"