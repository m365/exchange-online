<#
.SYNOPSIS
Based on message trace logs move unread messages from inbox to deleted items.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.ReadWrite
#>

# filter unread messages
$msgTraceResultsWithReadTag = Import-Csv C:\Temp\msgTraceResultsWithReadTag.csv | Where-Object { $_.IsRead -eq $false }
$msgTraceResultDeleteMsgResult = $msgTraceResultsWithReadTag | ForEach-Object {
    $trace = $_
    try {
        # move the message to deleted items
        $null = Move-MgUserMessage -UserId $trace.recipientaddress -MessageId $trace.MbxMessageId -BodyParameter @{DestinationId = 'deleteditems' } -ErrorAction stop
        $null = Add-Member -InputObject $trace -NotePropertyName 'Deleted' -NotePropertyValue $true -Force
    } catch {
        $null = Add-Member -InputObject $trace -NotePropertyName 'Deleted' -NotePropertyValue 'NotFound' -Force
    }
}
$msgTraceResultDeleteMsgResult | Export-Csv .\msgTraceResultDeleteMsgResult.csv -NoTypeInformation -Encoding utf8
