<#
.SYNOPSIS
Get message trace logs with a tag which recipients have forwarding inbox rule set up.

Requires Exchange Online management module connection https://learn.microsoft.com/en-us/powershell/exchange/connect-to-exchange-online-powershell?view=exchange-ps
#>

# get recipients that reside in the tenant
$msgTraceResultsWithResidency = Import-Csv 'C:\Temp\msgTraceResultsWithResidency.csv' -Encoding utf8 | Where-Object { $_.TenantResident -eq $true }

$mbxsWithFwdInboxRule = $msgTraceResultsWithResidency | ForEach-Object {
    $recipient = $_
    # filter rules with enabled redirect/forward action
    $forwRule = Get-InboxRule -Mailbox $recipient.recipientAddress | 
    Where-Object { $_.enabled -eq $true -and ($_.ForwardAsAttachmentTo.count -gt 0 -or $_.ForwardTo.count -gt 0 -or $_.RedirectTo.count -gt 0) }
    $null = Add-Member -input $recipient -NotePropertyName 'redirectRule' -NotePropertyValue (($forwRule | Measure-Object).count -gt 0)
    $recipient
}
$mbxsWithFwdInboxRule | Export-Csv C:\Temp\msgTraceRecipientsWithInboxRuleForwarding.csv -NoTypeInformation -Encoding utf8
