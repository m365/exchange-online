<#
.SYNOPSIS
Get message trace logs with a read tag.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.Read
#>

# get recipients that reside in the tenant
$msgTraceResultsWithResidency = Import-Csv 'C:\Temp\msgTraceResultsWithResidency.csv' -Encoding utf8 | Where-Object { $_.TenantResident -eq $true }
$msgTraceResultsWithReadTag = $msgTraceResultsWithResidency | ForEach-Object {
    $trace = $_
    # search the message
    $msg = Get-MgUserMessage -UserId $trace.recipientaddress -Filter "internetmessageid eq '$($trace.'messageId')'" -Property IsRead, InternetMessageId, subject, ReceivedDateTime
    if ($msg -ne $null) {
        $null = Add-Member -InputObject $trace -NotePropertyName 'MbxMessageId' -NotePropertyValue $($msg.Id) -Force
        $null = Add-Member -InputObject $trace -NotePropertyName 'IsRead' -NotePropertyValue $($msg.IsRead) -Force
    } else {
        $null = Add-Member -InputObject $trace -NotePropertyName 'MbxMessageId' -NotePropertyValue '' -Force
        $null = Add-Member -InputObject $trace -NotePropertyName 'IsRead' -NotePropertyValue 'NotFound' -Force
    }
    $trace
}
$msgTraceResultsWithReadTag | Export-Csv C:\Temp\msgTraceResultsWithReadTag.csv -NoTypeInformation -Encoding utf8
