<#
.SYNOPSIS
Based on message trace logs move read messages from inbox to junk folder.

Requires Microsoft Graph module connection https://learn.microsoft.com/en-us/powershell/microsoftgraph/app-only?view=graph-powershell-1.0&tabs=azure-portal
Requires application permission Mail.ReadWrite
#>

# filter read message (once again)
$msgTraceResultsWithReadTag = Import-Csv C:\Temp\msgTraceResultsWithReadTag.csv | Where-Object { $_.IsRead -eq $true }
$msgTraceResultJunkDeleteMsgResult = $msgTraceResultsWithReadTag | ForEach-Object {
    $trace = $_
    try {
        # search the message
        $msg = Get-MgUserMessage -UserId $trace.recipientaddress -Filter "internetmessageid eq '$($trace.'messageId')'" -Property IsRead, InternetMessageId, subject, ReceivedDateTime
        # move the message to deleted items
        $null = Move-MgUserMessage -UserId $trace.recipientaddress -MessageId $msg.MbxMessageId -BodyParameter @{DestinationId = 'deleteditems' } -ErrorAction stop
        $null = Add-Member -InputObject $trace -NotePropertyName 'MovedToJunk' -NotePropertyValue $true -Force
    } catch {
        $null = Add-Member -InputObject $trace -NotePropertyName 'MovedToJunk' -NotePropertyValue 'NotFound' -Force
    }
    $trace
}
$msgTraceResultJunkDeleteMsgResult | Export-Csv C:\Temp\msgTraceResultJunkDeleteMsgResult.csv -NoTypeInformation -Encoding utf8
