<#
.SYNOPSIS
Get message trace logs with recipient's mailbox residency tag.

Requires Exchange Online management module connection https://learn.microsoft.com/en-us/powershell/exchange/connect-to-exchange-online-powershell?view=exchange-ps
#>

$msgTraceResults = Import-Csv C:\Temp\msgTrace.csv -Encoding utf8

# get recipients that reside in the tenant
$msgTraceResultsWithResidency = $msgTraceResults | ForEach-Object { 
    # filter mailboxes that have no forwarding addresses or keep copies of forwarded messages
    if ((get-mailbox $_.recipientAddress -recipienttypedetails usermailbox, sharedmailbox -ErrorAction silentlycontinue | Where-Object {
        ([string]::IsNullOrEmpty($_.ForwardingAddress) -and [string]::IsNullOrEmpty($_.ForwardingSMTPAddress)) -or $_.DeliverToMailboxAndForward -eq $true
            }) -ne $null ) {
        $null = Add-Member -InputObject $_ -NotePropertyName 'TenantResident' -NotePropertyValue $true
    } else {
        $null = Add-Member -InputObject $_ -NotePropertyName 'TenantResident' -NotePropertyValue $false
    }
    $_
}
$msgTraceResultsWithResidency | Export-Csv C:\Temp\msgTraceResultsWithResidency.csv -NoTypeInformation -Encoding utf8