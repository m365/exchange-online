<#
.SYNOPSIS
Merge multiple message trace log files into single one.
#>

$workDirPath = 'C:\Temp'
$mergedMsgTrace = Get-ChildItem -Path $workDirPath -Filter 'MsgTrace_*.log' | Select-Object -ExpandProperty FullName | Import-Csv -Encoding utf8 
$mergedMsgTrace | Export-Csv "$workDirPath\MsgTrace_merged.csv" -NoTypeInformation -Append -Encoding utf8