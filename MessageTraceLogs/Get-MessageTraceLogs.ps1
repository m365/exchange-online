<#
.SYNOPSIS
Get complete Message trace for the time specified.
Modify config variables below.

Requires Exchange Online management module connection https://learn.microsoft.com/en-us/powershell/exchange/connect-to-exchange-online-powershell?view=exchange-ps

.EXAMPLE
.\Get-MesssageTraceLogs.ps1
#>



$global:ErrorActionPreference = 'stop'

#------ config
$LogFolderPath = 'C:\Temp'
$MsgTraceFolderPath = 'C:\Temp'
$MsgTraceDaysToPull = 1 # number of days in the past to process
$MsgTraceProcessNumberOfDaysBefore = 0 # set to 0 for today's data to start with, set to 1 for yesterday's data to start with

#Connect-ExchangeOnline

function Get-MsgTraceTimeSlots {

    # generate UTC hour sequnce
    $Hours = (0..23) | ForEach-Object { '{0:d2}' -f $_ }
    $Minutes = @('00:00')

    $TimesToCollect = @()
    for ($i = $MsgTraceDaysToPull - 1; $i -ge 0; $i--) {
        foreach ($Hour in $Hours) {
            foreach ($Minute in $Minutes) {
                if ($i -eq 0 -and $Hour -eq '23' -and $Minute -eq $Minutes[($Minutes.count - 1)]) {
                    $time = $(Get-Date -Format yyyy-MM-dd -Date (Get-Date).AddDays(-$MsgTraceProcessNumberOfDaysBefore - $i)) + ' ' + $Hour + ':' + $Minute
                    $TimesToCollect += $time

                    # get the complete last 15 minutes <23:45 - 00:00) of the last day
                    $time = $(Get-Date -Format yyyy-MM-dd -Date (Get-Date).AddDays(-$MsgTraceProcessNumberOfDaysBefore - $i + 1)) + ' 00:00:00'
                    $TimesToCollect += $time
                } else {
                    $time = $(Get-Date -Format yyyy-MM-dd -Date (Get-Date).AddDays(-$MsgTraceProcessNumberOfDaysBefore - $i)) + ' ' + $Hour + ':' + $Minute
                    $TimesToCollect += $time
                }
                #Write-MsgTraceLog 'DEBUG' $time
            }
        }
    }
    return $TimesToCollect
}

function Get-FileNameTimeSlotString {
    param (
        [parameter(Mandatory = $true)]    
        [String[]] $dateTimes
    )
    
    $startTime = ($dateTimes[0] -replace ' ', '-T') -replace ':', '#'
    $endTime = ($dateTimes[1] -replace ' ', '-T') -replace ':', '#'

    return ($startTime + '_' + $endTime)
}

function Get-ProcessedMsgObject {
    param (
        [parameter(Mandatory = $true)]    
        $message
    )

    return (New-Object psobject -Property @{
            SenderAddress    = "$($message.SenderAddress)".trim()
            RecipientAddress = "$($message.recipientaddress)".trim()
            Received         = (([datetime]$message.Received).ToLocalTime().GetDateTimeFormats('s'))[0]
            ReceivedUTC      = (([datetime]$message.Received).ToUniversalTime().GetDateTimeFormats('s'))[0]
            Status           = $message.Status
            ToIP             = $message.ToIP
            FromIP           = $message.FromIP
            Subject          = "$($message.Subject)".trim()
            Size             = $message.Size
            MessageTraceId   = $message.MessageTraceID.Guid
            MessageId        = $message.MessageID
            RecipientDomain  = "$(($message.RecipientAddress -split '@')[1])".trim()
            SenderDomain     = "$(($message.SenderAddress -split '@')[1])".trim()
        })
}

function Get-MsgTrace {
    param (
        [parameter(Mandatory = $false,
            Position = 0)]
        [DateTime] $StartDate = ((Get-Date).AddDays(-1)),

        [parameter(Mandatory = $false,
            Position = 1)]
        [DateTime] $EndDate = (Get-Date)
    )

    $messageTrace = New-Object System.Collections.ArrayList
    $page = 1
    do {

        # keep StartDate and EndDate in UTC format as on the input via using ToLocalTime(), avoid implicit conversion

        <# convert output received date time format
        $message = Get-MessageTrace | select -First 1
        $message.Received                      # Friday, July 7, 2023 5:39:03 AM # utc
        $message.Received.ToLocalTime()        # Friday, July 7, 2023 7:39:03 AM
        $message.Received.ToUniversalTime()    # Friday, July 7, 2023 3:39:03 AM # INVALID
        ---
        $message.Received                      # Friday, July 7, 2023 7:39:03 AM # local time
        $message.Received.ToLocalTime()        # Friday, July 7, 2023 7:39:03 AM
        $message.Received.ToUniversalTime()    # Friday, July 7, 2023 5:39:03 AM # VALID
        #>
        $messageTraceBatch = Get-MessageTrace -StartDate ($StartDate).ToLocalTime() -EndDate ($EndDate).ToLocalTime() -Page $page -PageSize 5000 | ForEach-Object { $_.received = $_.received.ToLocalTime(); $_ }
        
        if ($messageTraceBatch -ne $null) {
            $null = $messageTrace.addrange($messageTraceBatch)
        }
        
        $page ++
    } while (($messageTraceBatch.Count % 5000 -eq 0) -and $messageTraceBatch.Count -ne 0 ) 
		
    $messageTrace    
}

function Write-MsgTraceLog {
    param (
        [string] $logLevel,    
        [string] $logMessage
    )
    
    "$(Get-Date -f o)|$logLevel|$logMessage" | Out-File "$LogFolderPath\$Global:LogFileName.log" -Encoding utf8 -Append
}

try {
    
    $Global:LogFileName = "MsgTraceLog_$(Get-Date -Format yyyy-MM-dd)"

    Write-MsgTraceLog 'INFO' 'MsgTrace job started.'
    
    #------------- get message trace

    $TimesToCollect = Get-MsgTraceTimeSlots

    for ($t = 0; $t -lt $TimesToCollect.Count - 1; $t++) {

        $timeSlotString = Get-FileNameTimeSlotString $TimesToCollect[$t], $TimesToCollect[$t + 1]

        # ex.: MsgTrace_2020-03-25-T00#00#00_2020-03-25-T00#15#00.log
        $ProcessedFileName = "$('MsgTrace_'+$timeSlotString+'.log')"
        $ProcessedFilePath = "$($MsgTraceFolderPath)/$ProcessedFileName"

        if (Test-Path $ProcessedFilePath) {
            Write-MsgTraceLog 'DEBUG' "Already have file $ProcessedFileName"
        } elseif ( (Get-Date).AddHours(-1).ToUniversalTime() -lt $TimesToCollect[$t]   ) {

            Write-MsgTraceLog 'DEBUG' "Too soon to pull $ProcessedFileName"
        } else {

            Write-MsgTraceLog 'DEBUG' "Processing $ProcessedFileName"

            # get msgtrace for a time slot
            try {
                
                # send request
                $msgTrace = $null
                $msgTrace = Get-MsgTrace -StartDate $TimesToCollect[$t] -EndDate $TimesToCollect[$t + 1]
            } catch {
                if (($_.exception.message -match 'Start date .* is greater than end date' -and ([datetime]$TimesToCollect[$t]).month -eq 3 -and (([datetime]$TimesToCollect[$t]).hour -eq 2) -or ([datetime]$TimesToCollect[$t]).hour -eq 3)) {
                
                    #summer time, ExO would fail on startdate > enddate
                    #known bug, missing 15 minutes in output 02:45-04:00
                    Write-MsgTraceLog 'WARNING' "Time slot skipped: Summer time $($TimesToCollect[$t]) - $($TimesToCollect[$t+1])"
                    $msgTrace = $null
                } else {
                    throw $_
                }
            }

            if ($msgTrace.count -eq 0) {

                # write empty file
                New-Item -ItemType file -Path $ProcessedFilePath                
            } else {
            
                $msgTrace | ForEach-Object {
                    $msg = $_
                    Get-ProcessedMsgObject $msg 
                    
                    #Get-ProcessedMsgObject $msg | ConvertTo-Json -Compress | Out-File $ProcessedFilePath -Append -Force -Encoding utf8
                } | Export-Csv $ProcessedFilePath -NoTypeInformation -Encoding utf8 
            }
        }
    }

    Write-MsgTraceLog 'INFO' 'MsgTraceBatch finished'
} catch {
    
    Write-MsgTraceLog 'Error' $_
    exit 1
} finally {
    Write-MsgTraceLog 'INFO' 'MsgTrace job ended.'
}